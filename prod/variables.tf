# ws nat_instance/variables.tf

variable "environment" {
  default = "prod"
}
variable "managed_by" {
  default = "nat_instance"
}
variable "access_ip" {}
