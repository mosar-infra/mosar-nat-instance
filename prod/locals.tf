# nat_instance/locals.tf

locals {
  security_groups = {
    nat_instance = {
      vpc_id      = data.aws_vpc.vpc.id
      name        = "nat_instance_sg"
      description = "security group for nat_instance access"
      ingress_cidr = {
        ssh = {
          from        = 22
          to          = 22
          protocol    = "tcp"
          cidr_blocks = var.access_ip
        }
        https = {
          from        = 443
          to          = 443
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
        http = {
          from        = 80
          to          = 80
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
      ingress_sg = {}
      egress = {
        ssh = {
          from        = 22
          to          = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        http = {
          from        = 80
          to          = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        https = {
          from        = 443
          to          = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}


locals {
  subnets = {
    private = data.aws_subnets.private.ids
    public  = data.aws_subnets.public.ids

  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
  }
}

locals {
  nodes = {
    nat_instance = {
      instance_type               = "t2.micro"
      subnet_ids                  = local.subnets.public
      vpc_security_group_ids      = module.security_groups.security_groups["nat_instance"].*.id
      associate_public_ip_address = true
      root_block_device_vol_size  = 10
      iam_instance_profile        = ""
      tags_name_prefix            = "nat_instance_"
      source_dest_check           = false
      user_data_file_path         = "./user_data_scripts/nat_instance.tpl"
      script_vars                 = {}
      private_ip                  = null
      key_name                    = data.aws_key_pair.mosar_server_ssh1.key_name

    }
  }
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
