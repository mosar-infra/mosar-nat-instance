# nat_instance/datasource

data "aws_vpc" "vpc" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnets" "private" {
  tags = {
    Environment = var.environment
    Name        = "*private*"
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  tags = {
    Environment = var.environment
    Name        = "*public*"
  }
}

data "aws_key_pair" "mosar_server_ssh1" {
  key_name = "mosar_server_ssh1"
  filter {
    name   = "tag:Environment"
    values = ["test"]
  }
}
