# ws nat_instance /main.tf


module "ec2" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-ec2.git?ref=tags/v1.1.0"
  ami_name_string = "amzn2-ami-hvm-2.0*"
  ami_owners      = ["amazon"]
  nodes           = local.nodes
  environment     = var.environment
  managed_by      = var.managed_by
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

resource "aws_route_table" "private_rt" {
  vpc_id = data.aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    # instance_id = module.ec2.aws_instance["nat_instance"].id
    network_interface_id = module.ec2.aws_instance["nat_instance"].primary_network_interface_id
  }
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
    Name        = "private_nat_instance_route_table"
  }
}

resource "aws_route_table_association" "private_nat_instance_rt_association" {
  count          = length(local.subnets.private)
  subnet_id      = local.subnets.private[count.index]
  route_table_id = aws_route_table.private_rt.id
}
