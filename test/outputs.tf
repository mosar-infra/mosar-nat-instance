#nat_instance/outputs

output "ec2" {
  value = module.ec2
}

output "security_groups" {
  value = module.security_groups
}
